package org.example;

import java.lang.Math;
public class Calculator {

    private double result;
    private String operation;
    private double num1;
    private double num2;

    private boolean nextNumberIsNum1 = true;

    private boolean isDate = false;
    private boolean isDate2 = false;

    //HEX
    private String hex;

    //Constructor
    public Calculator(){
        this.result = 0;
        this.operation = "";
        this.num1 = 0;
        this.num2 = 0;
        this.hex = "";
    }

    //Operations
    public double sqrt(double num1){
        this.result = Math.sqrt(num1);
        return result;
    }


    public double sum(double num1,double num2){
        this.result = num1 + num2;
        return result;
    }

    public double min(double num1,double num2){
        this.result = num1 - num2;
        return result;
    }

    public double mul(double num1,double num2){
        this.result = num1 * num2;
        return result;
    }

    public double div(double num1, double num2){
        this.result = num1 / num2;
        return result;
    }

    public double cylinder(double num1, double num2){
        this.result = Math.PI*num2*num2*num1;
        return result;
    }

    public double cone(double num1, double num2){
        this.result = (Math.PI*num2*num2*num1)*1/3;
        return result;
    }

    public double factorial(int num) {
        if (num == 0 || num == 1) {
            return 1;
        } else {
            double result = 1;
            for (int i = 2; i <= num; i++) {
                result *= i;
            }
            return result;
        }
    }

    public int fibonacci(int num) {
        if (num == 0) {
            return 0;
        } else if (num == 1) {
            return 1;
        } else {
            int fib1 = 0;
            int fib2 = 1;
            int fibonacci = 0;
            for (int i = 2; i <= num; i++) {
                fibonacci = fib1 + fib2;
                fib1 = fib2;
                fib2 = fibonacci;
            }
            return fibonacci;
        }
    }

    public String doubleToHexadecimal(double value) {
        // Converte o valor double para uma representação binária
        //long longValue = Double.doubleToRawLongBits(value);
        //String binaryValue = Long.toBinaryString(longValue);
        int binaryValueInt = (int) value;
        String binaryValue = Integer.toString(binaryValueInt);

        // Converte a representação binária para um valor inteiro
        int intValue = Integer.parseInt(binaryValue, 2);

        // Converte o valor inteiro para hexadecimal
        String hexadecimalValue = Integer.toHexString(intValue).toUpperCase();

        return hexadecimalValue;
    }



    public String hexadecimalToBinary(String hexadecimal) {
        try {
            // Converte a representação hexadecimal em um valor decimal
            int decimalValue = Integer.parseInt(hexadecimal, 16);

            // Converte o valor decimal em uma representação binária
            String binaryValue = Integer.toBinaryString(decimalValue);

            return binaryValue;
        } catch (NumberFormatException e) {
            // Se houver um erro ao converter, retorne um valor de erro
            return "Erro na conversão";
        }
    }



    //Getters & Setters
    public void reset(){
        this.result = 0;
        this.num1 = 0;
        this.num2 = 0;
        this.operation = "";
        this.nextNumberIsNum1 = true;
        this.isDate = false;
        this.isDate2 = false;
        this.hex = "";
    }

    //HEX getter and setter
    public String getHex() {
        return hex;
    }

    public void setHex(String hex) {
        this.hex = hex;
    }

    public double getResult() {
        return this.result;
    }

    public void setResult(double result){
        this.result = result;
    }

    public String getOperation(){
        return this.operation;
    }

    public void setOperation(String operation){
        this.operation = operation;
    }

    public double getNum1(){
        return this.num1;
    }

    public double getNum2(){
        return this.num2;
    }

    public void setNum1(double num1){
        this.num1 = num1;
    }

    public void setNum2(double num2){
        this.num2 = num2;
    }

    public void setNextNumberIsNum1(boolean bool){this.nextNumberIsNum1 = bool;}

    public boolean getIsNextNumberIsNum1(){return this.nextNumberIsNum1;}
}
